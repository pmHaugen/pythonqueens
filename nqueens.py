class QueenHandler:
    global N
    N = 8
    def printBoard(board):
        for i in range(N):
            for j in range(N):
                print(board[i][j], end='   ')
            print("\n")

    def checkLoc(board, x, y):
        for i in range(y):
            if board[x][i] == 1:
                return False
        for i, j in zip(range(x, -1, -1), range(y, -1, -1)):
            if board[i][j] == 1:
                return False
        for i, j in zip(range(x, N, 1), range(y, -1, -1)):
            if board[i][j] == 1:
                return False
        return True

    def findSolution(board, queen, all):
        if queen>=N:
            if all == False:
                print("\n")
                QueenHandler.printBoard(board)
                return False
            else:
                return True

        for i in range(N):
            if QueenHandler.checkLoc(board, i, queen):
                board[i][queen] = 1
 
                if QueenHandler.findSolution(board, queen + 1, all) == True:
                    return True
                board[i][queen] = 0
 
        return False

board = [[0 for i in range(N)] for j in range(N)]
inp = input("Press 1 for one solutions, press 2 for all solutions:   ")
if inp == "1":
    QueenHandler.findSolution(board, 0, True)
    QueenHandler.printBoard(board)
elif inp == "2":
    QueenHandler.findSolution(board, 0, False)