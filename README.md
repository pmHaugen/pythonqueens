# N Queens
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)


![alt picture](queen.png)
## Requirments
python3

## Run
in project root directory:
```
python3 nqueens.py
```

## Contributing
- Paal Marius Haugen

## License
[MIT](docs/LICENSE.md)
